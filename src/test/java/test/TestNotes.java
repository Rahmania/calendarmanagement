package test;




import java.util.Date;
import logic.Note;
import logic.NoteManager;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ronella
 */
public class TestNotes {
    
 public TestNotes() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testAddNote(){
        NoteManager manager = new NoteManager();

       manager.addNotes(new Note("note",new Date(), "😀",0));
        assertEquals(0, manager.getNotes().size());
        manager.addNotes(new Note("note",new Date(), "😀",0));
        assertEquals(1,manager.getNotes().size());
        assertEquals("Web Applications", manager.getNotes().get(0).getTxt());
        
    }
}
    

