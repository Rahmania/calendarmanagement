package de.hsm.controller;

import de.hsm.persistent.Persistent;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
//import static jdk.nashorn.internal.runtime.Debug.id;
import logic.Note;
import logic.NoteManager;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Ronella
 */
@Controller
public class IndexController {
    SimpleDateFormat df = new SimpleDateFormat("yyyy-mm-dd");
    
    @GetMapping("/")
    public String index(){
        return "AddNotes";
    }
    
    @GetMapping("/Notesprogram")
    public ModelAndView createNotes(Note Note, BindingResult bindingResult){
        NoteManager manager = new NoteManager();
        ModelAndView mv = new ModelAndView();
        Map<String, String> names = new HashMap<>();
        names.put("😀", "😀");
        names.put("😞", "😞");
        mv.setViewName("Notesprogram");
        mv.addObject("notes", manager.getNotes());
        mv.addObject("names", names);
     return mv;
    }
  /*/   @RequestMapping(value="studyprogram",method=RequestMethod.POST )
    public ModelAndView  storeStudyprogram(@Valid Note note
           ){
        NoteManager manager = new NoteManager();
        manager.addNotes(note);        
        ModelAndView mv = new ModelAndView();
        mv.setViewName("studyprogram");
        
        mv.addObject("notes", manager.getNotes());
        return mv;
}*/
      @PostMapping("/Notesprogram")
      public ModelAndView storeNotes(@Valid Note note,
         BindingResult bindingResult){
         NoteManager manager = new NoteManager(); 
         ModelAndView mv = new ModelAndView();
       if (!bindingResult.hasErrors()){
          if (manager.existsNote(note.getTxt())){
                FieldError fieldError = new FieldError("note", "txt", 
                note.getTxt(), true, null, null, 
                "Note name must be unique.");
                bindingResult.addError(fieldError);
            }
       else {
               manager.addNotes(note);
            }
        }
       
        mv.setViewName("Notesprogram");
        Map<String, String> names = new HashMap<>();
        names.put("😀", "😀");
        names.put("😞", "😞");
        mv.addObject("names", names);
        mv.addObject("notes", manager.getNotes()); 
         return mv;
    }   
    
    @RequestMapping("/delete/{id}")
    public ModelAndView deleteNote(@PathVariable int id,Note note){      
       NoteManager manager = new NoteManager(); 
        ModelAndView mv = new ModelAndView();
       System.out.println(id);
       manager.deleteNote(id);
     mv.setViewName("Notesprogram");
        Map<String, String> names = new HashMap<>();
        names.put("😀", "😀");
        names.put("😞", "😞");
        mv.addObject("names", names);
        mv.addObject("notes", manager.getNotes()); 
         return mv;
   }
}
 
 
