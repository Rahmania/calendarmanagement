package de.hsm.persistent;

import static java.time.Clock.system;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import logic.Note;
import logic.NoteManager;

public class Persistent {

    private static Persistent instance = new Persistent();
    public final  List<Note> notes;
    public static final AtomicInteger count = new AtomicInteger(0); 
    public static int id;
    
    private Persistent() {
        notes = new ArrayList<>();
    }

    public static Persistent getInstance() {
         if (instance == null) {
            instance = new Persistent();
        }
        return instance;
    }
    public void persist(Object o) {
         if (o.getClass().equals(Note.class)){
             Note note = new Note();
           // note.setId(id);
             notes.add((Note)o);
        } 
         else {
            throw new RuntimeException("Class not supported " + o.getClass().getName());
        }
    }
       
    public List<Note> getNotes(){
        return notes;
    }
    public Note find(String name){
        for (Note note: notes){
          if (note.getTxt().equals(name)){
             return note;
            }
        }
        throw new NoteNotFoundException();
    }
    public void addNotes(Note note){ 
      note.setId(id++);
      notes.add(note);
    }
    public void deleteNote(int id){
     for(int i = 0;i<notes.size();i++){ 
          if(notes.get(i).getId() == id ){ 
          notes.remove(i);
          break;
        } 
        }}          
}

