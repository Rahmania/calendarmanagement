/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic;

import java.util.Date;
import javax.validation.constraints.NotEmpty;

/*package de.hsm.logic;*/




/**
 *
 * @author Ronella
 */
public class Note  {
     @NotEmpty
   private String txt;
   private Date date; 
   private int id;
   
   private String radioButtonVertical;
    
    public Note() {
    }
    public Note(String txt , Date date,String radioButtonVertical,int id) {
        this.txt = txt;
        this.date = date;
        this.radioButtonVertical=radioButtonVertical;
        this.id=id;
       
    }
    
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id =id;
        
    }
    public String getRadioButtonVertical() {
        return radioButtonVertical;
    }
    public void setRadioButtonVertical(String radioButtonVertical) {
        this.radioButtonVertical = radioButtonVertical;
    }
    public String getTxt() {
        return txt;
    }
    public void setTxt(String txt) {
        this.txt = txt;
    }
    public Date getDate() {
        return date;
    }
    /**
     *
     * @param date
    
     */
    public void setDate(Date date) {
        this.date = date;
    }   
}
