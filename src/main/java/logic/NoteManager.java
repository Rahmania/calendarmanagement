/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic;

import de.hsm.persistent.NoteNotFoundException;
import de.hsm.persistent.Persistent;
import de.hsm.persistent.QueryName;
import java.util.List;

/**
 *
 * @author Ronella
 */

public class NoteManager {
     public void addNotes(Note note){
        Persistent.getInstance().addNotes(note); 
    }
  
      public void deleteNote(int id){
        Persistent.getInstance().deleteNote(id);
    }
    public List<Note> getNotes(){
        return Persistent.getInstance().getNotes();
}
    public boolean existsNote(String name){
        try{
            Persistent.getInstance().find(name);
            return true;
        } 
        catch (NoteNotFoundException e){
            return false;
        }
        
}

}
