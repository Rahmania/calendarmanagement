<!DOCTYPE html>
<html>
    <head>
        <title>Library</title>
        <meta charset="windows-1252">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="/webjars/bootstrap/3.3.7-1/css/bootstrap.min.css" rel="stylesheet"/>
        <script src="/webjars/jquery/3.3.1-1/jquery.min.js"></script>
        <script src="/webjars/bootstrap/3.3.7-1/js/bootstrap.min.js"></script>
    </head>
    <body>
        <#macro template>
         <div class="container" >

            <div class="row" style="margin-bottom: 5px;">
                <div class="col-sm-3">
                </div>
                <div class="col-sm-9">
                    <h1 style="background-color: #FFFF00">ADD NOTES</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <nav class="navbar navbar-default" role="navigation">
                        <ul class="nav nav-pills nav-stacked">
                            
                            <li><a href="/Notesprogram">Add Notes</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col-sm-9">
                    <@content/>
                </div>
            </div>
        </div>        
        </#macro>
        <#macro content>This is content from base template</#macro>
    </body>
</html>