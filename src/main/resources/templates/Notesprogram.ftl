<#import "/spring.ftl" as spring/>
<#include "template.ftl">

   
<#macro content>

    <form action="/Notesprogram" method="POST" >

        <div class="form-group" >

            <label for="txt" >Description</label>
            <@spring.formInput "note.txt" "class='form-control'"/>
            <@spring.showErrors "<br>" "bg-danger"/><br>
        </div>
      <div class="form-group">
            <label for="date">Date(dd/mm/yyyy)</label>
            <@spring.formInput  "note.date" "class='form-control'"/>
            <@spring.showErrors "<br>" "bg-danger"/>
          
  
    <div class="form-group">
                    <label for="radioButtonVertical">How do you Feel?</label><br>
                    <@spring.formRadioButtons "note.radioButtonVertical" names "<br>"/>
                    <@spring.showErrors "<br>" "text-danger"/>
                    </div>


        <button type="submit" action="/addID" class="btn btn-primary">Save</button>
    </form><br>


   <table class="table table-striped table-hover table-responsive ">
        

<#list notes >
        <tr>
           <th>id</th>
            <th>Description</th>
            <th>Date</th>
             <th>Mood</th>
           <th>Delete</th>
            </tr>
        <#items as note>
        <tr>
            <td>${note.id}</td>

            <td>${note.txt}</td>
            <td>${note.date?string('dd.MM.yyyy HH:mm:ss')}</td>
            <td>  ${note.radioButtonVertical!}</td>
<form  method="GET" action="/delete/${note.id}">
 <td> <button type="submit" class="btn btn-primary" >dEL</button>
  </td>
</form>
    
        </tr>
        </#items>
        </#list>
    </table>



</#macro>
<@template/>